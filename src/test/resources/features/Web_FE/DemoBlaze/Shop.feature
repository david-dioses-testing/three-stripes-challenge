@Testing_WebFE @Shop
Feature: Shop

  @Documentation_Imperative
  Scenario Outline: Search, add, remove products and purchase (Imperative)
    Given Customer navigates to Demo Online Shop
    When he selects "Phones" category
    Then he validates that sample item for "Phones" category loaded correctly
    When he selects "Laptops" category
    Then he validates that sample item for "Laptops" category loaded correctly
    When he selects "Monitors" category
    Then he validates that sample item for "Monitors" category loaded correctly
    When he selects "Laptops" category
    When he navigates to <product 1> product
    Then he validates that Product Page for <product 1> product loaded correctly
    When he adds current product to cart
    When he clicks on "Cart" option from navigation bar
    Then he validates that Cart Page loaded correctly
    Then he validates that Cart contains <product 1> costing <price 1>
    When he clicks on "Home" option from navigation bar
    When he selects "Laptops" category
    When he navigates to <product 2> product
    Then he validates that Product Page for <product 2> product loaded correctly
    When he adds current product to cart
    When he clicks on "Cart" option from navigation bar
    Then he validates that Cart Page loaded correctly
    Then he validates that Cart contains <product 2> costing <price 2>
    When he removes <product 2> product from Cart
    Then he validates that <product 2> product was removed from Cart
    When he clicks on Place Order button in Cart
    Then he validates that Place Order Popup loaded correctly
    When he enters <Client Name> into "Name" webform field in Place Order popup
    When he enters <Client Country> into "Country" webform field in Place Order popup
    When he enters <Client City> into "City" webform field in Place Order popup
    When he enters <Client CC> into "Credit card" webform field in Place Order popup
    When he enters <Client CC Month> into "Month" webform field in Place Order popup
    When he enters <Client CC Year> into "Year" webform field in Place Order popup
    When he clicks on "Purchase" button in Place Order popup
    Then he validates that Successful Purchase Popup loaded correctly
    When he stores the "Id" displayed in Successful Purchase as "Purchase ID"
    When he stores the "Amount" displayed in Successful Purchase as "Purchase Amount"
    Then he validates that in Successful Purchase, "Amount" is "<price 1> USD"
    When he clicks on OK button in Successful Purchase Popup
    Then he validates that Successful Purchase Popup closed correctly

    Examples:
      | product 1      | price 1 | product 2     | price 2 | Client Name   | Client Country | Client City      | Client CC          | Client CC Month | Client CC Year |
      | "Sony vaio i5" | 790     | "Dell i7 8gb" | 700     | "Adi Dassler" | "Germany"      | "Herzogenaurach" | "0000111122223333" | "09"            | "22"           |

  @Documentation_Declarative
  Scenario Outline: Search, add, remove products and purchase (Declarative)
    Given Customer navigates to Demo Online Shop
    When he navigates to "Phones" category
    When he navigates to "Laptops" category
    When he navigates to "Monitors" category
    When he adds a "Laptops" - <product 1> costing <price 1>
    When he adds a "Laptops" - <product 2> costing <price 2>
    When he removes <product 2> from Cart
    When he places order for given customer and validate amount is "<price 1> USD"
      | Name        | <Client Name>     |
      | Country     | <Client Country>  |
      | City        | <Client City>     |
      | Credit card | <Client CC>       |
      | Month       | <Client CC Month> |
      | Year        | <Client CC Year>  |

    Examples:
      | product 1      | price 1 | product 2     | price 2 | Client Name   | Client Country | Client City      | Client CC          | Client CC Month | Client CC Year |
      | "Sony vaio i5" | 790     | "Dell i7 8gb" | 700     | "Adi Dassler" | "Germany"      | "Herzogenaurach" | "0000111122223333" | "09"            | "22"           |