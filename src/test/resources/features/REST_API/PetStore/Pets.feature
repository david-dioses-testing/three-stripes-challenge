@Testing_RESTAPI @Pets
Feature: Pets

  @Documentation_Imperative
  Scenario: Get available pets, create new pet, update added pet and remove modified pet (Imperative)
    Given Customer defines base url for REST API requests: "https://petstore.swagger.io/v2"
    When he defines "Get" REST API request type
    When he defines "/pet/findByStatus" REST API request URI path
    When he defines REST API request query parameters
      | status | available |
    When he SENDS THE REST API REQUEST
    Then he validates REST API response status code is: 200
    Then he validates that all REST API response nodes matching "findAll{pet -> 1==1}.status" jsonpath equal: "available"
    When he defines "Post" REST API request type
    When he defines "/pet" REST API request URI path
    When he uses "NewPet" file json for REST API request body, replacing jsonpaths matches by values
      | $.status      | available |
      | $.name        | Yaku      |
      | $.tags.[0].id | 7         |
    When he defines REST API request headers
      | Content-Type | application/json |
    When he SENDS THE REST API REQUEST
    Then he validates REST API response status code is: 200
    When he stores "id" from REST API response body in "Added pet ID"
    When he defines "Get" REST API request type
    When he defines "/pet/{petId}" REST API request URI path
    When he adds stored "Added pet ID" as "petId" REST API request path parameter
    When he SENDS THE REST API REQUEST
    Then he validates REST API response status code is: 200
    When he stores the REST API response body as "Added pet Json"
    When he defines "Put" REST API request type
    When he defines "/pet" REST API request URI path
    When he uses "Added pet Json" stored json for REST API request body, replacing jsonpaths matches by values
      | $.status | sold |
    When he defines REST API request headers
      | Content-Type | application/json |
    When he SENDS THE REST API REQUEST
    Then he validates REST API response status code is: 200
    When he defines "Get" REST API request type
    When he defines "/pet/{petId}" REST API request URI path
    When he adds stored "Added pet ID" as "petId" REST API request path parameter
    When he SENDS THE REST API REQUEST
    Then he validates REST API response status code is: 200
    Then he validates that REST API response node matching "status" jsonpath equals: "sold"
    When he defines "Delete" REST API request type
    When he defines "/pet/{petId}" REST API request URI path
    When he adds stored "Added pet ID" as "petId" REST API request path parameter
    When he SENDS THE REST API REQUEST
    Then he validates REST API response status code is: 200
    When he defines "Get" REST API request type
    When he defines "/pet/{petId}" REST API request URI path
    When he adds stored "Added pet ID" as "petId" REST API request path parameter
    When he SENDS THE REST API REQUEST
    Then he validates REST API response status code is: 404
    Then he validates that REST API response node matching "message" jsonpath equals: "Pet not found"


  @Documentation_Declarative
  Scenario: Get available pets, create new pet, update added pet and remove modified pet (Declarative)
    Given Customer defines base url for REST API requests: "https://petstore.swagger.io/v2"
    When he gets "available" pets and assert expected result
    When he posts a new "available" pet to the store and assert new pet added
      | $.name        | Yaku |
      | $.tags.[0].id | 7    |
    When he updates this pet status to "sold" and assert status updated
    When he deletes this pet and assert deletion

