package generic;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import util.FileUtil;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*;
import static net.serenitybdd.screenplay.targets.Target.the;

public class GenericActions {

    private static Target getTargetGivenElementNameAndLocator(String elementName, String elementLocator) {
        return the(elementName).locatedBy(elementLocator);
    }

    public static void actorNavigatesToPage(Actor actor, String pageName, Class pageClass) {
        actor.wasAbleTo(Task.where("{0} opens the " + pageName + " page",
                Open.browserOn().the(pageClass)));
    }

    public static void actorAttemptsToFillTextBox(String elementName, String elementLocator, String text, String actionDescription) {
        actorAttemptsToFillTextBox(getTargetGivenElementNameAndLocator(elementName, elementLocator), text, actionDescription);
    }

    public static void actorAttemptsToFillTextBox(Target target, String text, String actionDescription) {
        theActorInTheSpotlight().attemptsTo(Task.where("{0} " + actionDescription, Enter.theValue(text).into(target)));
    }

    public static void actorAttemptsToClick(String elementName, String elementLocator, String actionDescription) {
        actorAttemptsToClick(getTargetGivenElementNameAndLocator(elementName, elementLocator), actionDescription);
    }

    public static void actorAttemptsToClick(Target target, String actionDescription) {
        theActorInTheSpotlight().attemptsTo(Task.where("{0} " + actionDescription, Click.on(target)));
    }

    public static void actorWaitsUntilVisible(String elementName, String elementLocator) {
        actorWaitsUntilVisible(getTargetGivenElementNameAndLocator(elementName, elementLocator));
    }

    public static void actorWaitsUntilVisible(Target target) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(target, isVisible()));
    }

    public static void actorWaitsUntilNotPresent(String elementName, String elementLocator) {
        actorWaitsUntilNotPresent(getTargetGivenElementNameAndLocator(elementName, elementLocator));
    }

    public static void actorWaitsUntilNotPresent(Target target) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(target, isNotPresent()));
    }

    public static void actorAssertsFieldTextContains(String elementName, String elementLocator, String containedText) {
        actorAssertsFieldTextContains(getTargetGivenElementNameAndLocator(elementName, elementLocator), containedText);
    }

    public static void actorAssertsFieldTextContains(Target target, String containedText) {
        theActorInTheSpotlight().attemptsTo(Ensure.that(target).text().isEqualTo(containedText));
    }

    public static void actorAssertsFieldTextMatches(String elementName, String elementLocator, String matchingRegEx) {
        actorAssertsFieldTextMatches(getTargetGivenElementNameAndLocator(elementName, elementLocator), matchingRegEx);
    }

    public static void actorAssertsFieldTextMatches(Target target, String matchingRegEx) {
        theActorInTheSpotlight().attemptsTo(Ensure.that(target).text().matches(matchingRegEx));
    }

    public static String actorGetsFieldText(String elementName, String elementLocator) {
        return actorGetsFieldText(getTargetGivenElementNameAndLocator(elementName, elementLocator));
    }

    public static String actorGetsFieldText(Target target) {
        return Text.of(target).viewedBy(theActorInTheSpotlight()).asString();
    }

    static String STORED_VALUES_LOCATION = "target/stored_data.txt";

    public static void actorRemembersValue(String storeKey, String storeValue) {
        theActorInTheSpotlight().remember(storeKey, storeValue);
        FileUtil.appendText(STORED_VALUES_LOCATION, storeKey + ":\n\t" + storeValue.replace("\n", "\n\t")+"\n");
    }

    public static String actorRecallsValue(String storeKey) {
        return theActorInTheSpotlight().recall(storeKey);
    }

    public static void actorAcceptsAlertWithMessage(String message) {
        BrowseTheWeb.as(theActorInTheSpotlight()).waitFor(ExpectedConditions.alertIsPresent());
        Alert alert = BrowseTheWeb.as(theActorInTheSpotlight()).getAlert();
        String alertText = alert.getText();
        alert.accept();
        theActorInTheSpotlight().attemptsTo(Ensure.that(alertText).contains(message));
    }
}
