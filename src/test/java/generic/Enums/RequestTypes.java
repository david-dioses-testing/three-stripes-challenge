package generic.Enums;

public class RequestTypes {
    public static final String GET = "Get";
    public static final String POST = "Post";
    public static final String PUT = "Put";
    public static final String DELETE = "Delete";
}
