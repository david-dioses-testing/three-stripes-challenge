package generic.RestAPI;

import generic.Enums.RequestTypes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.serenitybdd.screenplay.rest.interactions.Put;
import net.serenitybdd.screenplay.rest.questions.RestQueryFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class RestAPIRequestEntity {
    private static String requestType;
    private static String requestBaseUrl;
    private static String requestURIPath;
    private static String requestBody;
    private static HashMap<String, Object> requestPathParams;
    private static HashMap<String, Object> requestQueryParams;
    private static HashMap<String, Object> requestHeaders;

    private static List<RestQueryFunction> getRestQueryFunction() {
        List<RestQueryFunction> restQueryFunctions = new ArrayList<RestQueryFunction>();
        restQueryFunctions.add(request -> request.pathParams(getRequestPathParams()));
        restQueryFunctions.add(request -> request.queryParams(getRequestQueryParams()));
        restQueryFunctions.add(request -> request.headers(getRequestHeaders()));
        restQueryFunctions.add(request -> request.body(getRequestBody()));
        return restQueryFunctions;
    }

    public static void sendRequest() {
        switch (getRequestType()) {
            case RequestTypes.GET:
                theActorInTheSpotlight().attemptsTo(Get.resource(getRequestURIPath()).with(getRestQueryFunction()));
                break;
            case RequestTypes.POST:
                theActorInTheSpotlight().attemptsTo(Post.to(getRequestURIPath()).with(getRestQueryFunction()));
                break;
            case RequestTypes.PUT:
                theActorInTheSpotlight().attemptsTo(Put.to(getRequestURIPath()).with(getRestQueryFunction()));
                break;
            case RequestTypes.DELETE:
                theActorInTheSpotlight().attemptsTo(Delete.from(getRequestURIPath()).with(getRestQueryFunction()));
                break;
        }
    }

    public static String getRequestType() {
        return requestType;
    }

    public static void setRequestType(String requestType) {
        RestAPIRequestEntity.requestType = requestType;
        cleanRequestData();
    }

    public static String getRequestBaseUrl() {
        return requestBaseUrl;
    }

    public static void setRequestBaseUrl(Actor actor, String requestBaseUrl) {
        actor.whoCan(CallAnApi.at(requestBaseUrl));
        RestAPIRequestEntity.requestBaseUrl = requestBaseUrl;
    }

    public static String getRequestURIPath() {
        return requestURIPath;
    }

    public static void setRequestURIPath(String requestURIPath) {
        RestAPIRequestEntity.requestURIPath = requestURIPath;
    }

    public static String getRequestBody() {
        return requestBody;
    }

    public static void setRequestBody(String requestBody) {
        RestAPIRequestEntity.requestBody = requestBody;
    }

    public static HashMap<String, Object> getRequestPathParams() {
        return requestPathParams;
    }

    public static void addRequestPathParams(String key, Object value) {
        if (RestAPIRequestEntity.requestPathParams == null) {
            initializeRequestPathParams();
        }
        RestAPIRequestEntity.requestPathParams.put(key, value);
    }

    public static void initializeRequestPathParams() {
        RestAPIRequestEntity.requestPathParams = new HashMap<String, Object>();
    }

    public static HashMap<String, Object> getRequestQueryParams() {
        return requestQueryParams;
    }

    public static void addRequestQueryParams(String key, Object value) {
        if (RestAPIRequestEntity.requestQueryParams == null) {
            initializeRequestQueryParams();
        }
        RestAPIRequestEntity.requestQueryParams.put(key, value);
    }

    public static void initializeRequestQueryParams() {
        RestAPIRequestEntity.requestQueryParams = new HashMap<String, Object>();
    }

    public static HashMap<String, Object> getRequestHeaders() {
        return requestHeaders;
    }

    public static void addRequestHeaders(String key, Object value) {
        if (RestAPIRequestEntity.requestHeaders == null) {
            initializeRequestHeaders();
        }
        RestAPIRequestEntity.requestHeaders.put(key, value);
    }

    public static void initializeRequestHeaders() {
        RestAPIRequestEntity.requestHeaders = new HashMap<String, Object>();
    }

    public static void cleanRequestData() {
        setRequestBody("");
        initializeRequestPathParams();
        initializeRequestQueryParams();
        initializeRequestHeaders();
    }
}
