package pages.DemoBlaze.Cart;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import generic.GenericActions;

public class PlaceOrderPopup extends PageObject {

    static String popupXpath = "//div[@id='orderModal']//div[@class='modal-content'][.//h5[text()='Place order']]";
    static Target POPUP = Target.the("Place Order Popup")
            .locatedBy(popupXpath);

    public static void validatePlaceOrderPopupLoadedCorrectly() {
        GenericActions.actorWaitsUntilVisible(POPUP);
    }

    public static void fillField(String fieldName, String inputText){
        GenericActions.actorAttemptsToFillTextBox(fieldName+" text box",popupXpath+"//div[@class='form-group'][./label[text()='"+fieldName+":']]/input", inputText, "fills '"+fieldName+"' webform field with: "+inputText);
    }

    public static void clickButton(String buttonName){
        GenericActions.actorAttemptsToClick(buttonName+" button",popupXpath+"//button[text()='"+buttonName+"']", "clicks on '"+buttonName+"' button");
    }
}