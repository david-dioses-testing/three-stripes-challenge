package pages.DemoBlaze.Cart;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import generic.GenericActions;

public class SuccessfulPurchasePopup extends PageObject {

    static String popupXpath = "//div[contains(@class,'sweet-alert ')][.//h2[text()='Thank you for your purchase!']]";
    static Target POPUP = Target.the("Successful Purchase Popup")
            .locatedBy(popupXpath);
    static Target PURCHASE_INFORMATION = Target.the("Purchase Information")
            .locatedBy(popupXpath + "//p[@class='lead text-muted ']");
    static Target OK_BUTTON = Target.the("OK Button")
            .locatedBy(popupXpath + "//button[text()='OK']");


    public static void validateSuccessfulPurchasePopupLoadedCorrectly() {
        GenericActions.actorWaitsUntilVisible(POPUP);
    }

    public static void validateSuccessfulPurchasePopupClosedCorrectly() {
        GenericActions.actorWaitsUntilNotPresent(POPUP);
    }

    public static void clickOKButton(){
        GenericActions.actorAttemptsToClick(OK_BUTTON,"clicks on OK button");
    }

    public static void validateFieldEquals(String fieldName, String fieldValue) {
        GenericActions.actorAssertsFieldTextMatches(PURCHASE_INFORMATION, "^(.*[\\n])*" + fieldName + ": " + fieldValue + "([\\n].*)*$");
    }

    public static void storeFieldValue(String fieldName, String storeKey) {
        String purchaseInfoRows = GenericActions.actorGetsFieldText(PURCHASE_INFORMATION);
        String fieldValue = "";
        for (String purchaseInfoRow : purchaseInfoRows.split("\n")) {
            if (purchaseInfoRow.startsWith(fieldName + ": ")) {
                fieldValue = purchaseInfoRow.substring(fieldName.length() + 2);
                break;
            }
        }
        GenericActions.actorRemembersValue(storeKey, fieldValue);
    }
}