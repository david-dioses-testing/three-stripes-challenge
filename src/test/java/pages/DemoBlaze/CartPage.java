package pages.DemoBlaze;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import generic.GenericActions;

public class CartPage extends PageObject {

    static String TABLE_HEADER_PRICE = "Price";
    static String TABLE_HEADER_TITLE = "Title";

    static Target PRODUCTS_TITLE_LABEL = Target.the("Products Title")
            .locatedBy("//h2[text()='Products']");
    static Target PLACE_ORDER_BUTTON = Target.the("Place Order")
            .locatedBy("//button[text()='Place Order']");

    private static Target PRODUCT_ROW(String title) {
        return Target.the("Product row")
                .locatedBy(getXpathForRowGivenTitle(title));
    }

    private static Target DELETE_PRODUCT(String title) {
        return Target.the("Delete Product")
                .locatedBy(getXpathForRowGivenTitle(title) + "//td/a[.//text()[.='Delete']]");
    }

    public static void validateCartPageLoadedCorrectly() {
        GenericActions.actorWaitsUntilVisible(PRODUCTS_TITLE_LABEL);
    }

    public static void placeOrder() {
        GenericActions.actorAttemptsToClick(PLACE_ORDER_BUTTON,"clicks on Place Order button");
    }

    public static void validateProductWasAdded(String title, int price) {
        GenericActions.actorWaitsUntilVisible(PRODUCT_ROW(title));
        GenericActions.actorWaitsUntilVisible("Product row with expected price", getXpathForRowGivenTitle(title) + getXpathForRowFilterGivenColumnAndValue(TABLE_HEADER_PRICE, "" + price));
    }

    public static void validateProductWasRemoved(String title) {
        GenericActions.actorWaitsUntilNotPresent(PRODUCT_ROW(title));
    }

    public static void removeProductFromCart(String title) {
        GenericActions.actorAttemptsToClick(DELETE_PRODUCT(title), "removes "+title+" Product");
    }

    private static String getXpathForRowGivenTitle(String title) {
        return "//table/tbody/tr" + getXpathForRowFilterGivenColumnAndValue(TABLE_HEADER_TITLE, title);
    }

    private static String getXpathForRowFilterGivenColumnAndValue(String columnName, String cellValue) {
        return "[./td[" + getXpathForProductsTableColumnNumber(columnName) + "][text()='" + cellValue + "']]";
    }

    private static String getXpathForProductsTableColumnNumber(String columnName) {
        return "count(//table/thead/tr/th[.//text()[.='" + columnName + "']]/preceding-sibling::*)+1";
    }
}