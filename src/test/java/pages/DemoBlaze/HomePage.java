package pages.DemoBlaze;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import generic.GenericActions;

@DefaultUrl("https://www.demoblaze.com/index.html")
public class HomePage extends PageObject {

    public static void selectCategory(String category) {
        GenericActions.actorAttemptsToClick("Products Category - " + category,
                "//a[@id='itemc'][text()='" + category + "']",
                "selects category '" + category + "'");
    }

    private static Target PRODUCT_LINK(String product) {
        return Target.the("Product: " + product)
                .locatedBy("//div[starts-with(@class,'card ')]//a[.//text()[.='" + product + "']]");
    }

    public static void navigateToProduct(String product) {
        GenericActions.actorAttemptsToClick(PRODUCT_LINK(product),
                "navigates to product '" + product + "'");
    }

    public static void validatesProductIsDisplayed(String product) {
        GenericActions.actorWaitsUntilVisible(PRODUCT_LINK(product));
    }

    public static void goToNavbarOption(String option) {
        GenericActions.actorAttemptsToClick("Navigation Bar option: " + option,
                "//div[@id='navbarExample']//a[.//text()[normalize-space(.)='" + option + "']]",
                "navigates to option '" + option + "'");
    }

    public static void validateSampleProductForCategoryVisible(String category) {
        String productName = "";
        switch (category) {
            case "Laptops":
                productName = "Sony vaio i5";
                break;
            case "Monitors":
                productName = "ASUS Full HD";
                break;
            case "Phones":
                productName = "Samsung galaxy s6";
                break;
            default:
                new AssertionError("Invalid Category");
                break;
        }
        GenericActions.actorWaitsUntilVisible(PRODUCT_LINK(productName));
    }
}
