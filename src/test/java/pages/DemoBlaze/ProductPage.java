package pages.DemoBlaze;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import generic.GenericActions;

public class ProductPage extends PageObject {

    static Target ADD_TO_CART_BUTTON = Target.the("Add to Cart button")
            .locatedBy("//a[text()='Add to cart']");

    private static Target productPageTitle(String product) {
        return Target.the("Product Page Title: " + product)
                .locatedBy("//h2[text()='" + product + "']");
    }

    public static void validateProductPageLoadedCorrectly(String product) {
        GenericActions.actorWaitsUntilVisible(productPageTitle(product));
    }

    public static void addProductToCart() {
        GenericActions.actorAttemptsToClick(ADD_TO_CART_BUTTON, "adds product to cart");
    }

    public static void acceptProductAddedAlert() {
        GenericActions.actorAcceptsAlertWithMessage("Product added");
    }

}
