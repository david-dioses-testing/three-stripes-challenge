package pages;

import net.serenitybdd.screenplay.Actor;
import generic.GenericActions;

public class NavigateTo {
    public static void specificPage(Actor actor, String pageName, Class pageClass) {
        GenericActions.actorNavigatesToPage(actor, pageName, pageClass);
    }
}
