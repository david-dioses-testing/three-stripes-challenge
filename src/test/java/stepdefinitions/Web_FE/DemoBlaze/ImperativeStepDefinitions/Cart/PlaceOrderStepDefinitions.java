package stepdefinitions.Web_FE.DemoBlaze.ImperativeStepDefinitions.Cart;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.Cart.PlaceOrderSteps;

public class PlaceOrderStepDefinitions {

    @Steps
    private PlaceOrderSteps placeOrderSteps;

    @Then("he validates that Place Order Popup loaded correctly")
    public void heValidatesThatPlaceOrderPopupLoadedCorrectly() {
        placeOrderSteps.validateThatPlacerOrderPopupLoadedCorrectly();
    }

    @When("he enters {string} into {string} webform field in Place Order popup")
    public void heEntersIntoWebformFieldOfPlaceOrderPopup(String inputText, String fieldName) {
        placeOrderSteps.fillPlaceOrderWebformFieldWithText(fieldName, inputText);
    }

    @When("he clicks on {string} button in Place Order popup")
    public void heClicksOnButtonInPlaceOrderPopup(String button) {
        placeOrderSteps.clickOnButtonInPlaceOrderPopup(button);
    }
}
