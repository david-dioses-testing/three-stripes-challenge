package stepdefinitions.Web_FE.DemoBlaze.ImperativeStepDefinitions.Cart;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.Cart.SuccessfulPurchaseSteps;

public class SuccessfulPurchaseStepDefinitions {

    @Steps
    private SuccessfulPurchaseSteps successfulPurchaseSteps;

    @Then("he validates that Successful Purchase Popup loaded correctly")
    public void heValidatesThatSuccessfulPurchasePopupLoadedCorrectly() {
        successfulPurchaseSteps.validateThatSuccessfulPurchasePopupLoadedCorrectly();
    }

    @Then("he validates that Successful Purchase Popup closed correctly")
    public void heValidatesThatSuccessfulPurchasePopupClosedCorrectly() {
        successfulPurchaseSteps.validateThatSuccessfulPurchasePopupClosedCorrectly();
    }

    @Then("he validates that in Successful Purchase, {string} is {string}")
    public void heValidatesThatInSuccessfulPurchaseIs(String fieldName, String fieldValue) {
        successfulPurchaseSteps.validateThatInSuccessfulPopupFieldEquals(fieldName, fieldValue);
    }

    @When("he stores the {string} displayed in Successful Purchase as {string}")
    public void heStoresTheDisplayedInSuccessfulPurchaseAs(String fieldName, String storeKey) {
        successfulPurchaseSteps.storeFieldValueFromSuccessfulPurchase(fieldName, storeKey);
    }

    @When("he clicks on OK button in Successful Purchase Popup")
    public void heClicksOnButtonInSuccessfulPurchasePopup() {
        successfulPurchaseSteps.clickOnOKButtonInSuccessfulPurchasePopup();
    }
}
