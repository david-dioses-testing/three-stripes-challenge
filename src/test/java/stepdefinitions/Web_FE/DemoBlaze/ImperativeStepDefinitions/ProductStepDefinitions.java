package stepdefinitions.Web_FE.DemoBlaze.ImperativeStepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.ProductSteps;

public class ProductStepDefinitions {

    @Steps
    private ProductSteps productSteps;

    @Then("he validates that Product Page for {string} product loaded correctly")
    public void heValidatesThatProductPageForProductLoadedCorrectly(String product) {
        productSteps.validateProductPageLoaded(product);
    }

    @When("he adds current product to cart")
    public void heAddsCurrentProductToCart() {
        productSteps.addProductToCart();
    }

}
