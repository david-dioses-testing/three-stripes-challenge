package stepdefinitions.Web_FE.DemoBlaze.ImperativeStepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.CartSteps;

public class CartStepDefinitions {

    @Steps
    private CartSteps cartSteps;

    @Then("he validates that Cart Page loaded correctly")
    public void heValidatesThatCartPageLoadedCorrectly() {
        cartSteps.validateCartPageLoaded();
    }

    @Then("he validates that Cart contains {string} costing {int}")
    public void heValidatesThatCartContainsProductCostingPrice(String product, int price) {
        cartSteps.validateProductRow(product, price);
    }

    @When("he removes {string} product from Cart")
    public void heRemovesProductProductFromCart(String product) {
        cartSteps.removeProductFromCart(product);
    }

    @Then("he validates that {string} product was removed from Cart")
    public void heValidatesThatProductProductWasRemovedFromCart(String product) {
        cartSteps.validateProductWasRemoved(product);
    }

    @When("he clicks on Place Order button in Cart")
    public void heClicksOnButtonInCart() {
        cartSteps.clickOnPlaceOrderInCart();
    }
}
