package stepdefinitions.Web_FE.DemoBlaze.ImperativeStepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Steps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.HomeSteps;

public class HomeStepDefinitions {

    @Steps
    private HomeSteps homeSteps;

    @Given("{actor} navigates to Demo Online Shop")
    public void navigateToDemoOnlineShop(Actor actor) {
        homeSteps.navigateToDemoOnlineShop(actor);
    }

    @When("he selects {string} category")
    public void selectsCategory(String category) {
        homeSteps.selectCategory(category);
    }

    @When("he navigates to {string} product")
    public void heNavigatesToProduct(String product) {
        homeSteps.navigateToProduct(product);
    }

    @When("he clicks on {string} option from navigation bar")
    public void heClicksOnOptionFromToolbar(String option) {
        homeSteps.selectOptionFromNavbar(option);
    }

    @Then("he validates that sample item for {string} category loaded correctly")
    public void heValidatesThatSampleItemForCategoryLoadedCorrectly(String category) {
        homeSteps.validateThatSampleItemForCategoryIsVisible(category);
    }
}
