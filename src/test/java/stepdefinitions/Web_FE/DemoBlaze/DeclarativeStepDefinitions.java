package stepdefinitions.Web_FE.DemoBlaze;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.Web_FE.DemoBlaze.DeclarativeSteps;

public class DeclarativeStepDefinitions {

    @Steps
    private DeclarativeSteps declarativeSteps;

    @When("he navigates to {string} category")
    public void heNavigatesToCategory(String category) {
        declarativeSteps.navigateToCategory(category);
    }

    @When("he adds a {string} - {string} costing {int}")
    public void heAddsAProductCostingPrice(String category, String product, int price) {
        declarativeSteps.addProduct(category, product, price);
    }

    @When("he removes {string} from Cart")
    public void heRemovesProductFromCart(String product) {
        declarativeSteps.removeProduct(product);
    }

    @When("he places order for: {string} -- {string} -- {string} -- {string} -- {string} -- {string} and validate amount is {string}")
    public void hePlacesOrderForClientNameClientCountryClientCityClientCCClientCCMonthClientCCYearAndValidateAmountIs(String name, String country, String city, String cc, String ccMonth, String ccYear, String amount) {
        declarativeSteps.hePlacesOrderAndValidateAmount(name, country, city, cc, ccMonth, ccYear, amount);
    }

    @When("he places order for given customer and validate amount is {string}")
    public void hePlacesOrderForGivenCustomerAndValidateAmountIs(String amount, DataTable dataTable) {
        declarativeSteps.hePlacesOrderAndValidateAmount(amount, dataTable);
    }
}
