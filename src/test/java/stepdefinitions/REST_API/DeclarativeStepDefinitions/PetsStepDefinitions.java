package stepdefinitions.REST_API.DeclarativeStepDefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.REST_API.DeclarativeSteps.PetsSteps;

public class PetsStepDefinitions {

    @Steps
    private PetsSteps petsSteps;

    @When("he gets {string} pets and assert expected result")
    public void heGetGetsPetsAndAssertExpectedResult(String status) {
        petsSteps.getPetsWithGivenStatusAndAssert(status);
    }

    @When("he posts a new {string} pet to the store and assert new pet added")
    public void hePostsANewPetToTheStoreAndAssertNewPetAdded(String status, DataTable dataTable) {
        petsSteps.postNewPetAndAssert(status, dataTable);
    }

    @When("he updates this pet status to {string} and assert status updated")
    public void heUpdatesThisPetStatusToAndAssertStatusUpdated(String status) {
        petsSteps.updatePetStatusAndAssert(status);
    }

    @When("he deletes this pet and assert deletion")
    public void heDeletesThisPetAndAssertDeletion() {
        petsSteps.deletePetAndAssert();
    }
}
