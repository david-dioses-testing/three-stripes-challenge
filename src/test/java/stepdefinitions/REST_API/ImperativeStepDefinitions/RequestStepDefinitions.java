package stepdefinitions.REST_API.ImperativeStepDefinitions;

import steps.REST_API.ImperativeSteps.RequestSteps;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Steps;

public class RequestStepDefinitions {

    @Steps
    private RequestSteps requestSteps;

    @Given("{actor} defines base url for REST API requests: {string}")
    public void customerDefinesBaseUrlForRESTAPIRequests(Actor actor, String baseUrl) {
        requestSteps.setRequestBaseUrl(actor, baseUrl);
    }

    @When("he defines {string} REST API request type")
    public void heDefinesRestApiRequestType(String requestType) {
        requestSteps.setRequestType(requestType);
    }

    @When("he defines {string} REST API request URI path")
    public void heDefinesRestApiRequestURIPath(String requestURIPath) {
        requestSteps.setRequestURIPath(requestURIPath);
    }

    @When("he defines REST API request headers")
    public void heDefinesRestApiRequestHeaders(DataTable dataTable) {
        requestSteps.addHeaders(dataTable);
    }

    @When("he defines REST API request query parameters")
    public void heDefinesRestApiRequestQueryParameters(DataTable dataTable) {
        requestSteps.addQueryParameters(dataTable);
    }

    @When("he defines REST API request path parameters")
    public void heDefinesRestApiRequestPathParameters(DataTable dataTable) {
        requestSteps.addPathParameters(dataTable);
    }

    @When("he adds stored {string} as {string} REST API request path parameter")
    public void heAddsStoredAsRESTAPIRequestPathParameter(String storeKey, String pathParameter) {
        requestSteps.addStoredValueAsPathParameter(storeKey, pathParameter);
    }

    @When("he uses {string} file json for REST API request body, replacing jsonpaths matches by values")
    public void heUsesFileJsonForRESTAPIRequestBodyReplacingValues(String jsonName, DataTable dataTable) {
        requestSteps.attachFileJsonToBody(jsonName, dataTable);
    }

    @When("he uses {string} stored json for REST API request body, replacing jsonpaths matches by values")
    public void heUsesStoredJsonForRESTAPIRequestBodyReplacingValues(String storeKey, DataTable dataTable) {
        requestSteps.attachStoredJsonToBody(storeKey, dataTable);
    }

    @When("he SENDS THE REST API REQUEST")
    public void heSendsTheRestApiRequest() {
        requestSteps.sendRequest();
    }
}
