package stepdefinitions.REST_API.ImperativeStepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.REST_API.ImperativeSteps.ResponseSteps;

public class ResponseStepDefinitions {

    @Steps
    private ResponseSteps responseSteps;

    @Then("he validates REST API response status code is: {int}")
    public void heValidatesRESTAPIResponseStatusCodeIs(int statusCode) {
        responseSteps.validateStatusCode(statusCode);
    }

    @Then("he validates that all REST API response nodes matching {string} jsonpath equal: {string}")
    public void heValidatesThatAllRESTAPIResponseNodesMatchingJsonpathEquals(String jsonPath, Object expectedEqualValue) {
        responseSteps.validateAllResponseNodesEqual(jsonPath, expectedEqualValue);
    }

    @Then("he validates that REST API response node matching {string} jsonpath equals: {string}")
    public void heValidatesThatRESTAPIResponseNodeMatchingJsonpathEquals(String jsonPath, Object expectedEqualValue) {
        responseSteps.validateResponseNodeEquals(jsonPath, expectedEqualValue);
    }

    @Then("he stores {string} from REST API response body in {string}")
    public void heStoresFromRESTAPIResponseBodyIn(String jsonPath, String storeKey) {
        responseSteps.storeNodeValueFromResponseBody(jsonPath, storeKey);
    }

    @When("he stores the REST API response body as {string}")
    public void heStoresTheRESTAPIResponseBody(String storeKey) {
        responseSteps.storeResponseBody(storeKey);
    }
}
