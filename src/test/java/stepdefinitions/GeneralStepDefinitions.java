package stepdefinitions;

import io.cucumber.java.Before;
import net.thucydides.core.annotations.Steps;
import steps.GeneralSteps;

public class GeneralStepDefinitions {

    @Steps
    private GeneralSteps generalSteps;

    @Before
    public void setTheStage() {
        generalSteps.setTheStage();
    }
}
