package util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

public class FileUtil {
    public static void appendText(String locationInsideProject, String textToAppend) {
        textToAppend += System.lineSeparator();
        try {
            Files.write(
                    Paths.get(locationInsideProject),
                    textToAppend.getBytes(), CREATE, APPEND
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(String locationInsideProject) {
        try {
            Files.deleteIfExists(Paths.get(locationInsideProject));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getFileStringGivenPath(String path) {
        List<String> fileLinesList = new ArrayList<>();
        try {
            fileLinesList = Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String fileString = "";
        for (String jsonLine : fileLinesList) {
            fileString += jsonLine + "\n";
        }
        return fileString;
    }
}
