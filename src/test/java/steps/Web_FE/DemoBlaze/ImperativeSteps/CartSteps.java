package steps.Web_FE.DemoBlaze.ImperativeSteps;

import net.thucydides.core.annotations.Step;
import pages.DemoBlaze.CartPage;

public class CartSteps {

    @Step
    public void validateCartPageLoaded() {
        CartPage.validateCartPageLoadedCorrectly();
    }

    @Step
    public void validateProductRow(String product, int price) {
        CartPage.validateProductWasAdded(product, price);
    }

    @Step
    public void removeProductFromCart(String product) {
        CartPage.removeProductFromCart(product);
    }

    @Step
    public void validateProductWasRemoved(String product) {
        CartPage.validateProductWasRemoved(product);
    }

    @Step
    public void clickOnPlaceOrderInCart() {
        CartPage.placeOrder();
    }
}
