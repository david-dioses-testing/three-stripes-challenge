package steps.Web_FE.DemoBlaze.ImperativeSteps.Cart;

import net.thucydides.core.annotations.Step;
import pages.DemoBlaze.Cart.SuccessfulPurchasePopup;

public class SuccessfulPurchaseSteps {

    @Step
    public void validateThatSuccessfulPurchasePopupLoadedCorrectly() {
        SuccessfulPurchasePopup.validateSuccessfulPurchasePopupLoadedCorrectly();
    }

    @Step
    public void validateThatSuccessfulPurchasePopupClosedCorrectly() {
        SuccessfulPurchasePopup.validateSuccessfulPurchasePopupClosedCorrectly();
    }

    @Step
    public void validateThatInSuccessfulPopupFieldEquals(String fieldName, String fieldValue) {
        SuccessfulPurchasePopup.validateFieldEquals(fieldName, fieldValue);
    }

    @Step
    public void storeFieldValueFromSuccessfulPurchase(String fieldName, String storeKey) {
        SuccessfulPurchasePopup.storeFieldValue(fieldName, storeKey);
    }

    @Step
    public void clickOnOKButtonInSuccessfulPurchasePopup() {
        SuccessfulPurchasePopup.clickOKButton();
    }
}
