package steps.Web_FE.DemoBlaze.ImperativeSteps.Cart;

import net.thucydides.core.annotations.Step;
import pages.DemoBlaze.Cart.PlaceOrderPopup;

public class PlaceOrderSteps {

    @Step
    public void validateThatPlacerOrderPopupLoadedCorrectly() {
        PlaceOrderPopup.validatePlaceOrderPopupLoadedCorrectly();
    }

    @Step
    public void fillPlaceOrderWebformFieldWithText(String fieldName, String inputText) {
        PlaceOrderPopup.fillField(fieldName, inputText);
    }

    @Step
    public void clickOnButtonInPlaceOrderPopup(String button) {
        PlaceOrderPopup.clickButton(button);
    }
}
