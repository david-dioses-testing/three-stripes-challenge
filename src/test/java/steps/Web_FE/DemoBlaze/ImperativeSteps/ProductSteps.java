package steps.Web_FE.DemoBlaze.ImperativeSteps;

import net.thucydides.core.annotations.Step;
import pages.DemoBlaze.ProductPage;

public class ProductSteps {

    @Step
    public void validateProductPageLoaded(String product) {
        ProductPage.validateProductPageLoadedCorrectly(product);
    }

    @Step
    public void addProductToCart() {
        ProductPage.addProductToCart();
        ProductPage.acceptProductAddedAlert();
    }
}
