package steps.Web_FE.DemoBlaze.ImperativeSteps;

import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Step;
import pages.NavigateTo;
import pages.DemoBlaze.HomePage;

public class HomeSteps {

    @Step
    public void navigateToDemoOnlineShop(Actor actor) {
        NavigateTo.specificPage(actor, "Demo Blaze", HomePage.class);
    }

    @Step
    public void selectCategory(String category) {
        HomePage.selectCategory(category);
    }

    @Step
    public void navigateToProduct(String product) {
        HomePage.navigateToProduct(product);
    }

    @Step
    public void selectOptionFromNavbar(String option) {
        HomePage.goToNavbarOption(option);
    }

    @Step
    public void validateThatSampleItemForCategoryIsVisible(String category) {
        HomePage.validateSampleProductForCategoryVisible(category);
    }
}
