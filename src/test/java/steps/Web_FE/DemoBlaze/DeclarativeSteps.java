package steps.Web_FE.DemoBlaze;

import generic.GenericMethods;
import steps.Web_FE.DemoBlaze.ImperativeSteps.CartSteps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.HomeSteps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.ProductSteps;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.Cart.PlaceOrderSteps;
import steps.Web_FE.DemoBlaze.ImperativeSteps.Cart.SuccessfulPurchaseSteps;

import java.util.Map;

public class DeclarativeSteps {

    @Steps
    private HomeSteps homeSteps;
    @Steps
    private ProductSteps productSteps;
    @Steps
    private CartSteps cartSteps;
    @Steps
    private PlaceOrderSteps placeOrderSteps;
    @Steps
    private SuccessfulPurchaseSteps successfulPurchaseSteps;

    @Step
    public void navigateToCategory(String category) {
        homeSteps.selectCategory(category);
        homeSteps.validateThatSampleItemForCategoryIsVisible(category);
    }

    @Step
    public void addProduct(String category, String product, int price) {
        homeSteps.selectOptionFromNavbar("Home");
        homeSteps.selectCategory(category);
        homeSteps.navigateToProduct(product);
        productSteps.validateProductPageLoaded(product);
        productSteps.addProductToCart();
        homeSteps.selectOptionFromNavbar("Cart");
        cartSteps.validateCartPageLoaded();
        cartSteps.validateProductRow(product, price);
    }

    @Step
    public void removeProduct(String product) {
        homeSteps.selectOptionFromNavbar("Cart");
        cartSteps.validateCartPageLoaded();
        cartSteps.removeProductFromCart(product);
        cartSteps.validateProductWasRemoved(product);
    }

    @Step
    public void hePlacesOrderAndValidateAmount(String name, String country, String city, String cc, String ccMonth, String ccYear, String amount) {
        homeSteps.selectOptionFromNavbar("Cart");
        cartSteps.validateCartPageLoaded();
        cartSteps.clickOnPlaceOrderInCart();
        placeOrderSteps.validateThatPlacerOrderPopupLoadedCorrectly();
        placeOrderSteps.fillPlaceOrderWebformFieldWithText("Name", name);
        placeOrderSteps.fillPlaceOrderWebformFieldWithText("Country", country);
        placeOrderSteps.fillPlaceOrderWebformFieldWithText("City", city);
        placeOrderSteps.fillPlaceOrderWebformFieldWithText("Credit card", cc);
        placeOrderSteps.fillPlaceOrderWebformFieldWithText("Month", ccMonth);
        placeOrderSteps.fillPlaceOrderWebformFieldWithText("Year", ccYear);
        placeOrderSteps.clickOnButtonInPlaceOrderPopup("Purchase");
        successfulPurchaseSteps.validateThatSuccessfulPurchasePopupLoadedCorrectly();
        successfulPurchaseSteps.storeFieldValueFromSuccessfulPurchase("Id", "Purchase ID");
        successfulPurchaseSteps.storeFieldValueFromSuccessfulPurchase("Amount", "Purchase Amount");
        successfulPurchaseSteps.validateThatInSuccessfulPopupFieldEquals("Amount", amount);
        successfulPurchaseSteps.clickOnOKButtonInSuccessfulPurchasePopup();
        successfulPurchaseSteps.validateThatSuccessfulPurchasePopupClosedCorrectly();
    }

    public void hePlacesOrderAndValidateAmount(String amount, DataTable dataTable) {
        homeSteps.selectOptionFromNavbar("Cart");
        cartSteps.validateCartPageLoaded();
        cartSteps.clickOnPlaceOrderInCart();
        placeOrderSteps.validateThatPlacerOrderPopupLoadedCorrectly();
        Map<Object, Object> customerDataSet = GenericMethods.convertDataTableToMap(dataTable);
        for (Object customerDataKey : customerDataSet.keySet()) {
            placeOrderSteps.fillPlaceOrderWebformFieldWithText(customerDataKey.toString(), GenericMethods.removeSurroundingDoubleQuotesFromString(customerDataSet.get(customerDataKey).toString()));
        }
        placeOrderSteps.clickOnButtonInPlaceOrderPopup("Purchase");
        successfulPurchaseSteps.validateThatSuccessfulPurchasePopupLoadedCorrectly();
        successfulPurchaseSteps.storeFieldValueFromSuccessfulPurchase("Id", "Purchase ID");
        successfulPurchaseSteps.storeFieldValueFromSuccessfulPurchase("Amount", "Purchase Amount");
        successfulPurchaseSteps.validateThatInSuccessfulPopupFieldEquals("Amount", amount);
        successfulPurchaseSteps.clickOnOKButtonInSuccessfulPurchasePopup();
        successfulPurchaseSteps.validateThatSuccessfulPurchasePopupClosedCorrectly();
    }
}
