package steps;

import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Step;

public class GeneralSteps {
    @Step
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }
}