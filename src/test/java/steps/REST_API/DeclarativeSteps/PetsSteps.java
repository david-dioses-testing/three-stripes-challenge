package steps.REST_API.DeclarativeSteps;

import generic.GenericMethods;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import steps.REST_API.ImperativeSteps.RequestSteps;
import steps.REST_API.ImperativeSteps.ResponseSteps;

import java.util.HashMap;
import java.util.Map;

public class PetsSteps {

    @Steps
    private RequestSteps requestSteps;
    @Steps
    private ResponseSteps responseSteps;

    @Step
    public void getPetsWithGivenStatusAndAssert(String status) {
        requestSteps.setRequestType("Get");
        requestSteps.setRequestURIPath("/pet/findByStatus");
        Map<Object, Object> queryParameters = new HashMap<>();
        queryParameters.put("status", status);
        requestSteps.addQueryParameters(queryParameters);
        requestSteps.sendRequest();
        responseSteps.validateStatusCode(200);
        responseSteps.validateAllResponseNodesEqual("findAll{pet -> 1==1}.status", status);
    }

    @Step
    public void postNewPetAndAssert(String status, DataTable dataTable) {
        requestSteps.setRequestType("Post");
        requestSteps.setRequestURIPath("/pet");
        HashMap<Object, Object> jsonPathsAndValuesToReplaceInBody = GenericMethods.convertDataTableToMap(dataTable);
        jsonPathsAndValuesToReplaceInBody.put("$.status", status);
        requestSteps.attachFileJsonToBody("NewPet", jsonPathsAndValuesToReplaceInBody);
        requestSteps.addContentTypeHeader("application/json");
        requestSteps.sendRequest();
        responseSteps.validateStatusCode(200);
        responseSteps.storeNodeValueFromResponseBody("id", "Added pet ID");
        requestSteps.setRequestType("Get");
        requestSteps.setRequestURIPath("/pet/{petId}");
        requestSteps.addStoredValueAsPathParameter("Added pet ID", "petId");
        requestSteps.sendRequest();
        responseSteps.validateStatusCode(200);
        responseSteps.storeResponseBody("Added pet Json");
    }

    @Step
    public void updatePetStatusAndAssert(String status) {
        requestSteps.setRequestType("Put");
        requestSteps.setRequestURIPath("/pet");
        Map<Object, Object> jsonPathsAndValuesToReplaceInBody = new HashMap<>();
        jsonPathsAndValuesToReplaceInBody.put("$.status", status);
        requestSteps.attachStoredJsonToBody("Added pet Json", jsonPathsAndValuesToReplaceInBody);
        requestSteps.addContentTypeHeader("application/json");
        requestSteps.sendRequest();
        responseSteps.validateStatusCode(200);
        requestSteps.setRequestType("Get");
        requestSteps.setRequestURIPath("/pet/{petId}");
        requestSteps.addStoredValueAsPathParameter("Added pet ID", "petId");
        requestSteps.sendRequest();
        responseSteps.validateStatusCode(200);
        responseSteps.validateResponseNodeEquals("status", status);
    }

    @Step
    public void deletePetAndAssert() {
        requestSteps.setRequestType("Delete");
        requestSteps.setRequestURIPath("/pet/{petId}");
        requestSteps.addStoredValueAsPathParameter("Added pet ID", "petId");
        requestSteps.sendRequest();
        responseSteps.validateStatusCode(200);
        requestSteps.setRequestType("Get");
        requestSteps.setRequestURIPath("/pet/{petId}");
        requestSteps.addStoredValueAsPathParameter("Added pet ID", "petId");
        requestSteps.sendRequest();
        responseSteps.validateStatusCode(404);
        responseSteps.validateResponseNodeEquals("message", "Pet not found");
    }
}