package steps.REST_API.ImperativeSteps;

import generic.GenericActions;
import generic.GenericMethods;
import generic.RestAPI.RestAPIRequestEntity;
import util.JsonUtil;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Step;
import org.json.JSONObject;

import java.util.Map;

public class RequestSteps {

    @Step
    public void setRequestBaseUrl(Actor actor, String requestBaseUrl) {
        RestAPIRequestEntity.setRequestBaseUrl(actor, requestBaseUrl);
    }

    @Step
    public void setRequestType(String requestType) {
        RestAPIRequestEntity.setRequestType(requestType);
    }

    @Step
    public void setRequestURIPath(String requestURIPath) {
        RestAPIRequestEntity.setRequestURIPath(requestURIPath);
    }

    @Step
    public void addContentTypeHeader(String contentType) {
        RestAPIRequestEntity.addRequestHeaders("Content-Type", contentType);
    }

    @Step
    public void addHeaders(Map<Object, Object> headersMap) {
        for (Object key : headersMap.keySet())
            RestAPIRequestEntity.addRequestHeaders(key.toString(), headersMap.get(key));
    }

    @Step
    public void addHeaders(DataTable dataTable) {
        Map<Object, Object> headersMap = GenericMethods.convertDataTableToMap(dataTable);
        addHeaders(headersMap);
    }

    @Step
    public void addPathParameters(Map<Object, Object> pathParametersMap) {
        for (Object key : pathParametersMap.keySet())
            RestAPIRequestEntity.addRequestPathParams(key.toString(), pathParametersMap.get(key));
    }

    @Step
    public void addPathParameters(DataTable dataTable) {
        Map<Object, Object> pathParametersMap = GenericMethods.convertDataTableToMap(dataTable);
        addPathParameters(pathParametersMap);
    }

    @Step
    public void addStoredValueAsPathParameter(String storeKey, String pathParameter) {
        String storedValue = GenericActions.actorRecallsValue(storeKey);
        RestAPIRequestEntity.addRequestPathParams(pathParameter, storedValue);
    }

    @Step
    public void addQueryParameters(Map<Object, Object> queryParametersMap) {
        for (Object key : queryParametersMap.keySet())
            RestAPIRequestEntity.addRequestQueryParams(key.toString(), queryParametersMap.get(key));
    }

    @Step
    public void addQueryParameters(DataTable dataTable) {
        Map<Object, Object> queryParametersMap = GenericMethods.convertDataTableToMap(dataTable);
        addQueryParameters(queryParametersMap);
    }

    @Step
    public void attachModifiedJsonToBody(JSONObject json, Map<Object, Object> jsonPathAndValuesToReplace) {
        for (Object key : jsonPathAndValuesToReplace.keySet())
            json = JsonUtil.replaceValueInJsonGivenJsonPath(json, key.toString(), jsonPathAndValuesToReplace.get(key));
        RestAPIRequestEntity.setRequestBody(json.toString());
    }

    @Step
    public void attachFileJsonToBody(String jsonName, Map<Object, Object> jsonPathAndValuesToReplace) {
        JSONObject json = JsonUtil.getRequestJsonObject(jsonName);
        attachModifiedJsonToBody(json, jsonPathAndValuesToReplace);
    }

    @Step
    public void attachFileJsonToBody(String jsonName, DataTable dataTable) {
        Map<Object, Object> jsonPathAndValuesToReplace = GenericMethods.convertDataTableToMap(dataTable);
        attachFileJsonToBody(jsonName, jsonPathAndValuesToReplace);
    }

    @Step
    public void attachStoredJsonToBody(String storeKey, Map<Object, Object> jsonPathAndValuesToReplace) {
        JSONObject json = new JSONObject(GenericActions.actorRecallsValue(storeKey));
        attachModifiedJsonToBody(json, jsonPathAndValuesToReplace);
    }

    @Step
    public void attachStoredJsonToBody(String storeKey, DataTable dataTable) {
        Map<Object, Object> jsonPathAndValuesToReplace = GenericMethods.convertDataTableToMap(dataTable);
        attachStoredJsonToBody(storeKey, jsonPathAndValuesToReplace);
    }

    @Step
    public void sendRequest() {
        RestAPIRequestEntity.sendRequest();
    }
}