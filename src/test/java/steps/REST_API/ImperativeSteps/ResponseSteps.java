package steps.REST_API.ImperativeSteps;

import generic.GenericActions;
import net.thucydides.core.annotations.Step;
import generic.RestAPI.RestAPIMethods;

public class ResponseSteps {
    @Step
    public void validateStatusCode(int statusCode) {
        RestAPIMethods.validateResponseStatusCode(statusCode);
    }

    @Step
    public void validateAllResponseNodesEqual(String jsonPath, Object expectedEqualValue) {
        RestAPIMethods.validateAllValuesOfResponseNodeEquals(jsonPath, expectedEqualValue);
    }

    @Step
    public void validateResponseNodeEquals(String jsonPath, Object expectedEqualValue) {
        RestAPIMethods.validateResponseNodeValueEquals(jsonPath, expectedEqualValue);
    }

    @Step
    public void storeNodeValueFromResponseBody(String jsonPath, String storeKey) {
        String valueToSave = RestAPIMethods.getNodeStringFromJsonResponseBody(jsonPath);
        GenericActions.actorRemembersValue(storeKey, valueToSave);
    }

    @Step
    public void storeResponseBody(String storeKey) {
        String responseBody = RestAPIMethods.getJsonResponseBody();
        GenericActions.actorRemembersValue(storeKey, responseBody);
    }
}