# Automation Testing Challenge
## _Solution Documentation_

This repository contains my solution to the challenge for the hiring process for a QA Engineer position.

**Serenity BDD Framework** with **Cucumber** has been used for this solution.

**Included** testing in this repository:

- **Web Frond End**
- **REST API**

Even when they have both been included here they **can be run separately**.

---
## Testing Scope

For **this initial solution**, only the tests specified in the Exercises have been automated and included in just 1 test or scenario per exercise.

For **both** Web Front End and REST API testing automation, we have used **2 approaches** for the **documentation**:

- **Imperative**: Each step in the cucumber file represents one unique action or validation done by the user. Works better for development or testing teams analysis as it is very detailed _(Atomic steps)_.
- **Declarative**: Each step in the cucumber file is actually a flow or a set of dependent actions or validations. Works better for management as it is more general _(Grouped steps)_.
    - Even when **Declarative** documentation has **more general steps**, in the Serenity Report after the tests run, you will be able to **expand its details to see the Imperative steps contained** on them.

The idea is to **decide which** of these **2 approaches** fits better the **team needs**. Steps from **both documentation** types can be **reusable** in **different tests**.

---
## Web Front End Testing - Exercise solution

**Feature/Cucumber file** was created with the flows that were required in the Exercise for **[Demo Blaze](https://demoblaze.com/)**.

_Location: /src/test/resources/features/Web_FE/DemoBlaze/Shop.feature_

This is the **feature name** and its **tags**
```gherkin
@Testing_WebFE @Shop
Feature: Shop
```
Test with **Imperative** Documentation _(Atomic Steps)_
```gherkin
@Documentation_Imperative
Scenario Outline: Search, add, remove products and purchase (Imperative)
Given Customer navigates to Demo Online Shop
When he selects "Phones" category
Then he validates that sample item for "Phones" category loaded correctly
When he selects "Laptops" category
Then he validates that sample item for "Laptops" category loaded correctly
When he selects "Monitors" category
Then he validates that sample item for "Monitors" category loaded correctly
When he selects "Laptops" category
When he navigates to <product 1> product
Then he validates that Product Page for <product 1> product loaded correctly
When he adds current product to cart
When he clicks on "Cart" option from navigation bar
Then he validates that Cart Page loaded correctly
Then he validates that Cart contains <product 1> costing <price 1>
When he clicks on "Home" option from navigation bar
When he selects "Laptops" category
When he navigates to <product 2> product
Then he validates that Product Page for <product 2> product loaded correctly
When he adds current product to cart
When he clicks on "Cart" option from navigation bar
Then he validates that Cart Page loaded correctly
Then he validates that Cart contains <product 2> costing <price 2>
When he removes <product 2> product from Cart
Then he validates that <product 2> product was removed from Cart
When he clicks on Place Order button in Cart
Then he validates that Place Order Popup loaded correctly
When he enters <Client Name> into "Name" webform field in Place Order popup
When he enters <Client Country> into "Country" webform field in Place Order popup
When he enters <Client City> into "City" webform field in Place Order popup
When he enters <Client CC> into "Credit card" webform field in Place Order popup
When he enters <Client CC Month> into "Month" webform field in Place Order popup
When he enters <Client CC Year> into "Year" webform field in Place Order popup
When he clicks on "Purchase" button in Place Order popup
Then he validates that Successful Purchase Popup loaded correctly
When he stores the "Id" displayed in Successful Purchase as "Purchase ID"
When he stores the "Amount" displayed in Successful Purchase as "Purchase Amount"
Then he validates that in Successful Purchase, "Amount" is "<price 1> USD"
When he clicks on OK button in Successful Purchase Popup
Then he validates that Successful Purchase Popup closed correctly

Examples:
| product 1      | price 1 | product 2     | price 2 | Client Name   | Client Country | Client City      | Client CC          | Client CC Month | Client CC Year |
| "Sony vaio i5" | 790     | "Dell i7 8gb" | 700     | "Adi Dassler" | "Germany"      | "Herzogenaurach" | "0000111122223333" | "09"            | "22"           |
```

Test with **Declarative** Documentation _(Grouped Steps)_
```gherkin
@Documentation_Declarative
Scenario Outline: Search, add, remove products and purchase (Declarative)
Given Customer navigates to Demo Online Shop
When he navigates to "Phones" category
When he navigates to "Laptops" category
When he navigates to "Monitors" category
When he adds a "Laptops" - <product 1> costing <price 1>
When he adds a "Laptops" - <product 2> costing <price 2>
When he removes <product 2> from Cart
When he places order for given customer and validate amount is "<price 1> USD"
| Name        | <Client Name>     |
| Country     | <Client Country>  |
| City        | <Client City>     |
| Credit card | <Client CC>       |
| Month       | <Client CC Month> |
| Year        | <Client CC Year>  |

Examples:
| product 1      | price 1 | product 2     | price 2 | Client Name   | Client Country | Client City      | Client CC          | Client CC Month | Client CC Year |
| "Sony vaio i5" | 790     | "Dell i7 8gb" | 700     | "Adi Dassler" | "Germany"      | "Herzogenaurach" | "0000111122223333" | "09"            | "22"           |
```

---
## REST API Testing - Exercise solution

**Feature/Cucumber file** was created with the flows that were required in the Exercise for **[Pet Store](https://petstore.swagger.io/)**. _(Input jsons required for the request body, are located inside "/requestjsons" folder)_

_Location: /src/test/resources/features/REST_API/PetStore/Pets.feature_

This is the **feature name** and its **tags**
```gherkin
@Testing_RESTAPI @Pets
Feature: Pets
```
Test with **Imperative** Documentation _(Atomic Steps)_
```gherkin
@Documentation_Imperative
Scenario: Get available pets, create new pet, update added pet and remove modified pet (Imperative)
Given Customer defines base url for REST API requests: "https://petstore.swagger.io/v2"
When he defines "Get" REST API request type
When he defines "/pet/findByStatus" REST API request URI path
When he defines REST API request query parameters
| status | available |
When he SENDS THE REST API REQUEST
Then he validates REST API response status code is: 200
Then he validates that all REST API response nodes matching "findAll{pet -> 1==1}.status" jsonpath equal: "available"
When he defines "Post" REST API request type
When he defines "/pet" REST API request URI path
When he uses "NewPet" file json for REST API request body, replacing jsonpaths matches by values
| $.status      | available |
| $.name        | Yaku      |
| $.tags.[0].id | 7         |
When he defines REST API request headers
| Content-Type | application/json |
When he SENDS THE REST API REQUEST
Then he validates REST API response status code is: 200
When he stores "id" from REST API response body in "Added pet ID"
When he defines "Get" REST API request type
When he defines "/pet/{petId}" REST API request URI path
When he adds stored "Added pet ID" as "petId" REST API request path parameter
When he SENDS THE REST API REQUEST
Then he validates REST API response status code is: 200
When he stores the REST API response body as "Added pet Json"
When he defines "Put" REST API request type
When he defines "/pet" REST API request URI path
When he uses "Added pet Json" stored json for REST API request body, replacing jsonpaths matches by values
| $.status | sold |
When he defines REST API request headers
| Content-Type | application/json |
When he SENDS THE REST API REQUEST
Then he validates REST API response status code is: 200
When he defines "Get" REST API request type
When he defines "/pet/{petId}" REST API request URI path
When he adds stored "Added pet ID" as "petId" REST API request path parameter
When he SENDS THE REST API REQUEST
Then he validates REST API response status code is: 200
Then he validates that REST API response node matching "status" jsonpath equals: "sold"
When he defines "Delete" REST API request type
When he defines "/pet/{petId}" REST API request URI path
When he adds stored "Added pet ID" as "petId" REST API request path parameter
When he SENDS THE REST API REQUEST
Then he validates REST API response status code is: 200
When he defines "Get" REST API request type
When he defines "/pet/{petId}" REST API request URI path
When he adds stored "Added pet ID" as "petId" REST API request path parameter
When he SENDS THE REST API REQUEST
Then he validates REST API response status code is: 404
Then he validates that REST API response node matching "message" jsonpath equals: "Pet not found"
```

Test with **Declarative** Documentation _(Grouped Steps)_
```gherkin
@Documentation_Declarative
Scenario: Get available pets, create new pet, update added pet and remove modified pet (Declarative)
Given Customer defines base url for REST API requests: "https://petstore.swagger.io/v2"
When he gets "available" pets and assert expected result
When he posts a new "available" pet to the store and assert new pet added
| $.name        | Yaku |
| $.tags.[0].id | 7    |
When he updates this pet status to "sold" and assert status updated
When he deletes this pet and assert deletion
```

---
## Testing proposal for a real project with Demo Blaze and Pet Store

- For **Web Front End Testing** of **[Demo Blaze](https://demoblaze.com/)** we should try to cover all the screens functionalities:
    - **Home Page:**
        - Footer has expected information
        - User can navigate through all the categories
        - All The navigation bar links work
        - User can navigate to products of all categories
        - User can add products to the Cart
    - **Contact:**
        - User is unable to send empty form
        - User can successfully sent form with valid data
    - **About Us:**
        - Video is available
    - **Cart:**
        - Added Items appear in Cart
        - User can delete Items from Cart
        - User is unable to Place Order without items
        - User can do a purchase with valid data
        - User is unable to do a purchase without required data
    - **Login**
        - User is able to login with valid credentials
        - User is unable to login with invalid or no credentials
    - **Sign Up**
        - User is able to sign up with non registered user
        - User is unable to sign up with registered user
        - User is unable to sign up with no information

- For **REST API Testing** of **[Pet Store](https://petstore.swagger.io/)** we should at least have 1 positive and 1 negative test for each of the services available.

    - **PET** Everything about your Pets

            POST    ​/pet​/{petId}​/uploadImage
            POST    ​/pet 
            PUT     ​/pet 
            GET     ​/pet​/findByStatus 
            GET     ​/pet​/findByTags
            GET     ​/pet​/{petId} 
            POST    ​/pet​/{petId} 
            DELETE  ​/pet​/{petId} 

    - **STORE** Access to Petstore orders

            GET     ​/store​/inventory 
            POST    ​/store​/order
            GET     ​/store​/order​/{orderId}
            DELETE  ​/store​/order​/{orderId} 

    - **USER** Operations about user

            POST    ​/user​/createWithList 
            GET     ​/user​/{username}
            PUT     ​/user​/{username}
            DELETE  ​/user​/{username} 
            GET     ​/user​/login 
            GET     ​/user​/logout 
            POST    ​/user​/createWithArray 
            POST    ​/user 

---
## Prerequisites

- Install **[JDK](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)**: Make sure to add to the Path and configure JAVA_HOME. _This solution has been tested with Java 8._
- Install **[Maven](https://maven.apache.org/install.html)**.
- Install the **desired browsers** for the Web Front End Testing.
- Install **[Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)**.
- Clone this **[repo](https://bitbucket.org/david-dioses-testing/three-stripes-challenge/)** in your machine.

---
## Run Instructions

First, you need to perform a clean install:

```sh
mvn clean install -DskipTests
```

Then, execute the tests:

```sh
mvn verify
```

**Command above** will execute all the tests in the repository, both **Web Front End and Rest API**. If you want to run tests from of **only one type**, you should add **"-Dcucumber.features"** parameter to the command with **"/src/test/resources/features/Web_FE"** or **"/src/test/resources/features/REST_API"** values

To execute only Web Front End Tests:

```sh
mvn verify -Dcucumber.features="/src/test/resources/features/Web_FE"
```

To execute only REST API Tests:

```sh
mvn verify -Dcucumber.features="/src/test/resources/features/REST_API"
```

By **default** the Web Front End Tests will run in **Chrome** browser, to **run with another** one, add **"-Dwebdriver.driver"** to the command with values like **"edge"** or **"firefox"**.

```sh
mvn verify -Dwebdriver.driver=edge
```

Also, if you want to run only some specific tests, you can also **filter by tags**, by adding **"-Dtags"** parameter with the desired tag.

```sh
mvn verify -Dtags="@Documentation_Declarative"
```

Finally, **open** the **Serenity Report** that will be generated in **"/target/site/serenity/index.html"** to see the results.

- Go to **"Test Results"** sub tab.
- **Click** on one **scenario link**.
- Go to the **bottom** of the screen and **expand** scenario **Steps**. You will be able to see the **status** of each step. You will even be able to **expand each steps details**, which is mostly useful in **Declarative** Scenarios.
    - For **Web Front End** tests, you will see a **screenshot** in **each step**.
    - For **REST API** tests, you will see the **request and response details** when you completely expand the step **"When he SENDS THE REST API REQUEST"** and click on the **"REST Query"** green button

Some scenarios have explicit **"store data"** steps. This explicitly stored data for some scenarios will be written and can be found after the runs in **"/target/stored_data.txt"**

---
## Architecture Summary

#### First layer

- **Cucumber/Feature** files: These files work as documentation in **gherkin syntax** (Sentences/Lines/Steps in human language with **Given** [Pre-condition], **When** [Action] and **Then** [Validation] prefixes)
    - Location: /src/test/resources/features

#### Second layer
- **Step Definitions** files: These files have **a method** for **each step in Cucumber files**
    - Location: /src/test/java/stepdefinitions
        - _Note: **Imperative** type (each Step Definition is an **only action or validation**) and **Declarative** type (each Step Definition represents a **piece of flow or functionality**) have been defined_

#### Third layer
- **Steps** files: These files have the **implemented methods** for **each Step Definition method**
    - Location: /src/test/java/steps
        - _Note: **Imperative** type (each Step is an **only action or validation**) and Declarative type (each Step is a **set of Imperative Steps**) have been defined_

#### Fourth layer
- **Page Object** files: These files contains the **web elements (Target)** and **methods specific** for **each page**
    - Location: /src/test/java/pages

#### Last layers
- **Generic**: Reusable generic methods and actions including **REST API request building**, **interactions with browser** and **variables parsing or conversion**.
    - Location: /src/test/java/generic
- **Util**: Reusable functions for **Files and Jsons** management.
    - Location: /src/test/java/util

---
## Room for Improvement

With more time, the **next actions** that could be done to improve this solution, would be:

- **Improve wording** of some steps and methods.
- **Encrypt sensitive data** like credit card to avoid being displayed in reports.
- Configure **different data** usage for different **environments**.
- Integrate with **Browserstack** (for **compatibility testing**).
- Integrate with **Jenkins** for (**CI/CD**)
